// Load dependencies
var gulp         = require('gulp'),
	sass         = require('gulp-sass');
	sassGlob     = require('gulp-sass-glob-import');
	sourcemaps   = require('gulp-sourcemaps'),
    jshint       = require('gulp-jshint'),
    uglify       = require('gulp-uglify'),
    rename       = require('gulp-rename'),
    concat       = require('gulp-concat'),
    notify       = require('gulp-notify'),
	del          = require('del'),
	ffmpeg       = require('gulp-fluent-ffmpeg'),
    livereload   = require('gulp-livereload');

gulp.task('clean', function() {
	del(['./dist']).then(paths => {
	    console.log('Deleted files and folders:\n', paths.join('\n'));
	});
});

// ==================
// Sass / css
// ==================

gulp.task('sass', function () {
  return gulp.src('./assets/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sassGlob()) // this let's you BULK IMPORT in your scss files, e.g. @import '*.scss';
    .pipe(sass({outputStyle: 'compressed'}).on('error',
		function(err) {
	        return notify().write(err);
	    }
	))
    .pipe(sourcemaps.write('')) // CHANGE this path (relative to gulp.dest path below) if you want sourcemap generated elsewhere
    .pipe(gulp.dest('./dist/css')) // CHANGE this path to your desired output (e.g. dist/styles)
	.pipe(notify({ message: 'sass completed' }))
    .pipe(livereload())

});

// ==================
// js
// ==================

gulp.task('scripts', function() {
	return gulp.src( './assets/js/*.js' )
		.pipe(sourcemaps.init())
		.pipe(concat('xplore.js'))
		// .pipe(rename({ suffix: '.min' }))
		.pipe(uglify().on('error', function(e){
		   console.log(e);
		}))
		.pipe(jshint())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist/js'))
		.pipe(notify({ message: 'SUCCESS:  Scripts' }))
});

// ==================
// mp3
// ==================

gulp.task('audio', function () {
	// transcode ogg files to mp3
	return gulp.src('assets/audio/*.wav')
	  .pipe(ffmpeg('mp3', function (cmd) {
		return cmd
		  .audioBitrate('128k')
		  .audioChannels(2)
		  .audioCodec('libmp3lame')
	  }))
	  .pipe(gulp.dest('dist/audio'))
	//   .pipe(notify({ message: 'SUCCESS:  Audio' }))
  });

// ==================
// Set default task order
// ==================

gulp.task('default', function() {
	gulp.start( 'sass', 'scripts', 'audio' );
});

// ==================
// Watch for changing files
// ==================

gulp.task('watch', function() {
    livereload.listen([{"port":"8000"}]);

	// Watch SCSS files
	gulp.watch('assets/scss/**/*.scss', ['sass']);

	// Watch JS files
	gulp.watch('assets/js/*.js', ['scripts']);

	// Watch JS files
	gulp.watch('assets/audio/*.wav', ['audio']);


	// Watch any files in dist/, reload on change
	gulp.watch(['dist/**','index.html']).on('change', livereload.changed);
});
