var backingOptions = document.querySelectorAll('input[name="backing"]');

for(var m = 0; m < backingOptions.length; m++) {
    backingOptions[m].addEventListener('click',function(){
        deselectBacking();
        this.setAttribute('data-status','selected');
        changeBacking();
    });
}

function deselectBacking() {
    for(var o = 0; o < backingOptions.length; o++) {
        backingOptions[o].setAttribute('data-status','');
    }
}

function stopAllBacking() {
    var backingAudio = document.querySelectorAll('audio.backing');

    for(var n = 0; n < backingAudio.length; n++) {
        backingAudio[n].pause();
    }
}

function changeBacking() {
    // get backing selection
    var backingSelection = document.querySelector('.backing [data-status="selected"]');
    var backing = document.querySelector('audio#'+backingSelection.id);
    // get scale
    stopAllBacking();

    if (backingSelection.id != 'backing_off') {
        backing.currentTime = 0;
        backing.play();
        backing.addEventListener('ended',playBacking);
    }

    function playBacking() {
        backing.currentTime = 0;
        backing.play();
    }

}
