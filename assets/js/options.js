var showHide = document.querySelector('.options .show-hide');
var optionContainer = document.querySelector('.option-container');

showHide.addEventListener('click',showHideOptions);

function showHideOptions(e) {
   if (e.target.className.indexOf(' hide') == -1) {
    console.log('a');

        // show
        showHide.classList.remove('show');
        showHide.classList.add('hide');
        optionContainer.classList.add('active');
   } else {
       // hide
       console.log('b');

       showHide.classList.remove('hide');
       showHide.classList.add('show');
       optionContainer.classList.remove('active');
   }
}