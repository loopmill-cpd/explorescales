function changeScale(e) {
    var scaleNotes = this.dataset.notes.split(',');
    var scaleButtons = document.querySelectorAll('.scales .scale');
    var notes = document.querySelectorAll('.note');
    var audioFiles = document.querySelectorAll('.audio-note');
    var keys = document.querySelectorAll('.note-label');
    var keyLabels = ['a','s','d','f','g','h','j','k','l','&semi;','&apos;','&bsol;','&crarr;']
    var keyCodes = ['65','83','68','70','71','72','74','75','76','186','222','220','13']

    for (var p=0;p<scaleButtons.length;p++) {
        scaleButtons[p].classList.remove('active');
    }
    e.target.classList.add('active');

    var ko = 0;
    for (var k=0;k<notes.length;k++) {
        if (scaleNotes.indexOf(notes[k].id.toString()) != -1) {
            notes[k].disabled = false;
            notes[k].innerHTML = keyLabels[ko];
            // set buttons
            notes[k].setAttribute('data-key',keyCodes[ko]);
            audioFiles[k].setAttribute('data-key',keyCodes[ko]);

            // set audio files

            ko ++;
        } else {
            notes[k].disabled = true;
            notes[k].innerHTML = '-';
            notes[k].setAttribute('data-key','-');
            audioFiles[k].setAttribute('data-key','-');
        }
    }

    for (var l=0;l<keys.length;l++) {
        if (scaleNotes.indexOf(keys[l].id.toString()) != -1) {
            keys[l].classList.remove('inactive');
        } else {
            keys[l].classList.add('inactive');
        }
    }

}

var scales = document.querySelectorAll('.scale');
for(var j=0;j<scales.length;j++) {
    scales[j].addEventListener('click',changeScale);
}