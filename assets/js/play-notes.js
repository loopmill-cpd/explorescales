function removeTransition(e) {
    if (e.propertyName !== 'transform') return;
    e.target.classList.remove('playing');
}

function playSound(e) {
    // console.dir(e.target.dataset.key);
    if (e.constructor.name == 'MouseEvent') {
        var audio = document.querySelector('audio[data-key="' + e.target.dataset.key + '"]');
        var key = document.querySelector('button[data-key="' + e.target.dataset.key + '"]');
    } else {
        var audio = document.querySelector('audio[data-key="' + e.keyCode + '"]');
        var key = document.querySelector('button[data-key="' + e.keyCode + '"]');
    }
 
    if (!audio) return;
    if (true == key.disabled) return;

    key.classList.add('playing');
    audio.currentTime = 0;
    audio.play();

    setTimeout(function(){ 
        key.classList.remove('playing');
    }, 1000);

}

var notes = document.querySelectorAll('.note');
for (var i=0;i<notes.length;i++) {
    notes[i].addEventListener('transitionend',removeTransition);
    notes[i].addEventListener('click',playSound);
}
window.addEventListener('keydown', playSound);

